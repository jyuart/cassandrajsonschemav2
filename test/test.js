const assert = require('assert');
const { removeFrozen, convertCassandraToJsonTypes } = require('../app/services/cassandraToSchemaService')
const { generateCassandraColumn } = require('../app/services/schemaToCassandraService')

describe('Convert', function() {
    describe('#removeFrozen()', function() {
      it('should return value without text and angle brackets', function() {
        assert.strictEqual(removeFrozen("frozen<text>"), "text")
      })
    })
})

describe('Convert', function() {
    describe('#convertCassandraToJsonTypes()', function() {
      it('should return object with type "string" when the parameter is "text"', async function() {
        await convertCassandraToJsonTypes("text").then(result => {
            assert.strictEqual(result.type, "string")
        })
      })
    })
})

describe('Convert', function() {
  describe('#generateCassandraColumn()', function() {
    it('should return "int" when parameter is "number"', function() {
      assert.strictEqual(generateCassandraColumn({ type: "number" }), "int")
    })
  })
})