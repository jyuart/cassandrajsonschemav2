const { jsonSchemaContent } = require("./readerService");
const { createTable, createUdt } = require("./dbService");
const { schemaValidation } = require("./schemaValidatorService");

if (schemaValidation) 
    jsonSchemaObject = JSON.parse(jsonSchemaContent);

const createCassandraColumns = function () {
  let result = "";
  schemaProperties = jsonSchemaObject.properties;
  schemaTitle = jsonSchemaObject.title;
  try {
    for (const [key, value] of Object.entries(schemaProperties))
      result += `"${key}" ${generateCassandraColumn(
        value,
        key,
        schemaTitle
      )},\r\n`;
    result += `${schemaTitle}_id uuid PRIMARY KEY`;
    createTable(jsonSchemaObject.title, result);
  } catch(error) {
    console.log(`Error ocurred while converting\n${error}`)
  }

};

const generateCassandraColumn = function (...args) {
  const value = args[0];
  if (value.hasOwnProperty("type")) {
    const type = value.type;
    switch (type) {
      case "number":
        return "int";
      case "boolean":
        return "boolean";
      case "array":
        return handleArray(value);
      case "string":
        return handleString(value);
      case "object":
        return "map<text, text>";
    }
  } else {
    const key = args[1];
    const tableName = args[2];
    const typeName = value.$ref;
    return handleUdt(typeName, key, tableName);
  }
};

const handleArray = function (value) {
  if (Array.isArray(value.items)) {
    return handleTuple(value);
  } else {
    let result = "";
    if (value.uniqueItems === true)
      result += "set<";
    else 
      result += "list<";
    result += generateCassandraColumn(value.items) + ">";
    return result;
  }
};

const handleTuple = function (value) {
  let result = "tuple<";
  value.items.forEach((type) => {
    result += `${generateCassandraColumn(type)}, `;
  });
  result.substring(0, result.length - 2) += ">";
  return result;
};

const handleString = function (value) {
  const stringFormat = value.stringFormat;
  switch (stringFormat) {
    case "date-time":
      return "timestamp";
    case "date":
      return "date";
    case "time":
      return "time";
    default:
      return "text";
  }
};

const handleUdt = function (value, key, tableName) {
  const ref = value.split("definitions/")[1];
  createUdtType(ref, key, tableName);
  return `frozen<${ref}>`;
};

async function createUdtType(ref, key, tableName) {
  const schemaDefinitions = jsonSchemaObject.definitions;
  udtObject = schemaDefinitions[ref];

  let result = "";
  udtProperties = udtObject.properties;
  for (const [key, value] of Object.entries(udtProperties))
    result += `"${key}" ${generateCassandraColumn(value)},\n`;
  createUdt(result, ref, table_name);
}

module.exports = { createCassandraColumns, generateCassandraColumn };
