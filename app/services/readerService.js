const fs = require("fs");
const { config } = require('../../config/dbConfig')

const jsonSchemaContent = fs.readFileSync(config.jsonSchema, "utf-8");

module.exports = { jsonSchemaContent }