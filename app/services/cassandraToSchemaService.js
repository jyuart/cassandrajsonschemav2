const { tableData, udtInfo } = require("../data/dbData");
const { handleJsonField } = require("./jsonToSchemaService");

const convertCassandraToJsonTypes = async function (cassandraType) {
  if (cassandraType.includes("frozen"))
    cassandraType = removeFrozen(cassandraType);

  if (cassandraType.startsWith("set") || cassandraType.startsWith("list"))
    return handleLists(cassandraType);
  else if (cassandraType.startsWith("map"))
    return handleMaps(cassandraType);
  else if (cassandraType.startsWith("tuple"))
    return handleTuples(cassandraType);

  switch (cassandraType) {
    case "boolean":
      return { type: "boolean" };
    case "ascii":
    case "date":
    case "uuid":
    case "timeuuid":
    case "text":
    case "varchar":
    case "inet":
    case "time":
    case "timestamp":
    case "blob":
      return { type: "string" };
    case "bigint":
    case "counter":
    case "decimal":
    case "double":
    case "duration":
    case "float":
    case "int":
    case "smallint":
    case "tinyint":
    case "varint":
      return { type: "number" };
    default:
      return await handleUDTs(cassandraType);
  }
};

const removeFrozen = function (cassandraType) {
  return cassandraType.replace("frozen<", "").replace(">", "");
};

const handleLists = async function (cassandraType) {
  const objectType = cassandraType.substring(
    cassandraType.indexOf("<") + 1,
    cassandraType.length - 1
  );
  return {
    type: "array",
    items: await convertCassandraToJsonTypes(objectType),
  };
};

const handleTuples = async function (cassandraType) {
  const tupleTypes = cassandraType
    .substring(cassandraType.indexOf("<") + 1, cassandraType.length - 1)
    .split(", ");
  result = {
    type: "object",
    properties: {},
  };
  for (i = 0; i < tupleTypes.length; i++) {
    result.properties[i] = await convertCassandraToJsonTypes(tupleTypes[i]);
  }
  return result;
};

const handleMaps = async function (cassandraType) {
  const mapTypes = cassandraType
    .substring(cassandraType.indexOf("<") + 1, cassandraType.length - 1)
    .split(", ");
  return {
    type: "object",
    properties: {
      key: await convertCassandraToJsonTypes(mapTypes[0]),
      value: await convertCassandraToJsonTypes(mapTypes[1]),
    },
  };
};

const handleUDTs = async function (cassandraType) {
  return new Promise((resolve) => {
    resolve(
      udtInfo(cassandraType).then(async (column) => {
        if (column) {
          let typeName = column["type_name"];
          typeName = {
            type: "object",
            properties: {},
          };
          const names = column["field_names"];
          const types = column["field_types"];
          for (const [index, field] of names.entries()) {
            typeName.properties[field] = await convertCassandraToJsonTypes(
              types[index]
            );
          };
          return typeName;
        }
      })
    );
  });
};

const createSchema = (tableName) =>
  tableData(tableName).then(async (columns) => {
    let finalSchema = {
      $schema: "http://json-schema.org/draft-04/schema#",
      type: "object",
      title: tableName,
      properties: {},
    };
    for (column of columns) {
      const columnName = column["column_name"];
      const initialType = column["type"];
      let JsonType;
      try {
        if (initialType === "text") {
          JsonType = await handleJsonField(tableName, columnName);
        } else {
          JsonType = await convertCassandraToJsonTypes(initialType);
        }
        finalSchema.properties[columnName] = JsonType;
      } catch (error) {
        console.log(`Error ocurred while converting. Final schema may be not full\n${error}`)
      }
    }
    return finalSchema;
  });

module.exports = { createSchema, removeFrozen, convertCassandraToJsonTypes };
