const { createSchema } = require("./cassandraToSchemaService");
const { allTables } = require("../data/dbData");
const fs = require("fs");

const startWriting = allTables().then((tables) => {
  tables.forEach((table) => {
    createSchema(table).then((result) => {
      fs.writeFile(`${table}_result.json`, JSON.stringify(result, null, 1), () =>
        console.log(`Table ${table} is converted to JSON schema and written to file`)
      );
    });
  });
}).catch(err => console.log(`Error ocurred while writing to file\n${err}`))

module.exports = { startWriting };
