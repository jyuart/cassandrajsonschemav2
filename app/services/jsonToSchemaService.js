const { firstyEntry } = require('../data/dbData')

const handleJsonField = async function (tableName, columnName) {
  return new Promise((resolve) => {
    resolve(
      firstyEntry(tableName, columnName).then((result) => {
        if (result) {
          const textField = result[0][columnName];
          try {
            const serializedData = JSON.parse(textField);
            return convertJsonToSchema(serializedData);
          } catch (err) {
            console.log(err);
            return { 
              type: "string" 
            };
          }
        }
      })
    );
  });
};

const convertJsonToSchema = function (serializedData) {
  let finalSchema = {
    type: "object",
    properties: {},
  };
  for (const [key, value] of Object.entries(serializedData)) {
    finalSchema.properties[key] = handleJsonTypes(value);
  }
  return finalSchema;
};

const handleJsonTypes = function (jsonValue) {
  jsonType = typeof jsonValue;
  switch (jsonType) {
    case "boolean":
    case "string":
    case "number":
      return { type: jsonType };
    case "object":
      if (!Array.isArray(jsonValue)) 
        return handleJsonObject(jsonValue);
      else 
        return handleJsonArray(jsonValue);
  }
};

const handleJsonArray = function (jsonValue) {
  if (isSameType) {
    let result = {
      type: "array",
      items: {},
    };
    result.items = handleJsonTypes(jsonValue[0])
    return result;
  } else {
    let result = {
      type: "array",
      items: [],
    };
    for (i = 0; i < jsonValue.length; i++) {
      result.items.push(handleJsonTypes(jsonValue[i]));
    }
    return result;
  }
};

const isSameType = function (jsonValue) {
  return jsonValue.every(el => typeof el === typeof jsonValue[0])
}

const handleJsonObject = function (jsonValue) {
  let result = {
    type: "object",
    properties: {},
  };
  for (const [key, value] of Object.entries(jsonValue)) {
    result.properties[key] = handleJsonTypes(value);
  }
  return result;
};

module.exports = { handleJsonField };
