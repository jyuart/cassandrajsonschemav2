const Ajv = require('ajv');
const { jsonSchemaContent } = require('./readerService')

jsonSchemaObject = JSON.parse(jsonSchemaContent)

var ajv = new Ajv({schemaId: 'auto'});
ajv.addMetaSchema(require('ajv/lib/refs/json-schema-draft-04.json'));

const schemaValidation = ajv.validateSchema(jsonSchemaObject)

if (ajv.errors) {
  console.log("There are some errors in JSON Schema:\n")
  console.log(ajv.errors)
}


module.exports = { schemaValidation }