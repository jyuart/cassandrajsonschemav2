const { dbConnection, client } = require("../data/dbConnection");

const createTable = (table_name, fields) => {
  let createKeyspaceScript = `CREATE KEYSPACE IF NOT EXISTS "${table_name}"
WITH REPLICATION = {
'class': 'SimpleStrategy',
'replication_factor': 1
}
AND DURABLE_WRITES = false;`;
  let createTableScript = `CREATE TABLE IF NOT EXISTS "${table_name}"."${table_name}"(${fields});`;
  dbConnection
    .then(() =>
      client
        .execute(createKeyspaceScript)
        .then(console.log(`Keyspace ${table_name} was succesfully created`))
        .then(() =>
          client
            .execute(createTableScript)
            .then(
              console.log(
                `Table ${table_name} was succesfully created in keyspace ${table_name}`
              )
            )
        )
    )
    .catch((err) =>
      console.log(`Error ocurred while creating table in database\n${err}`)
    );
};

const createUdt = (fields, type_name, table_name) => {
  let createKeyspaceScript = `CREATE KEYSPACE IF NOT EXISTS "${table_name}"
WITH REPLICATION = {
'class': 'SimpleStrategy',
'replication_factor': 1
}
AND DURABLE_WRITES = false;`;
  let createTypeScript = `CREATE TYPE IF NOT EXISTS "${table_name}"."${type_name}"(\n${fields}\n)`;
  console.log(script2);
  dbConnection
    .then(() =>
      client
        .execute(createKeyspaceScript)
        .then(console.log(`Keyspace ${type_name} was succesfully created`))
        .then(() => {
          client
            .execute(createTypeScript)
            .then(
              console.log(
                `Type ${type_name} was succesfully created in keyspace ${table_name}`
              )
            );
        })
    )
    .catch((err) =>
      console.log(`Error ocurred while creating UDT in database\n${err}`)
    );
};

module.exports = { createTable, createUdt };
