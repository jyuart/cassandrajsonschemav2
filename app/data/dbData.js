const { dbConnection, client } = require("./dbConnection");
const { config } = require("../../config/dbConfig");

const tableData = (table_name) =>
  dbConnection
    .then(() =>
      client
        .execute(
          `SELECT column_name, type FROM system_schema.columns WHERE keyspace_name = '${config.keyspace_name}' AND table_name='${table_name}';`
        )
        .then((result) => result.rows)
    )
    .catch((err) => console.log(`Error ocurred while trying to get table data\n${err}`));

const udtInfo = (type_name) =>
  dbConnection
    .then(() =>
      client
        .execute(
          `SELECT * FROM system_schema.types WHERE keyspace_name='${config.keyspace_name}' AND type_name='${type_name}';`
        )
        .then((result) => result.rows[0])
    )
    .catch((err) => console.log(`Error ocurred while trying to get UDT info\n${err}`));

const allTables = () =>
  dbConnection
    .then(() =>
      client
        .execute(
          `SELECT table_name FROM system_schema.tables WHERE keyspace_name = '${config.keyspace_name}';`
        )
        .then((result) => result.rows.map((t) => t.values()[0]))
    )
    .catch((err) => console.log(`Error ocurred while trying to get all tables names\n${err}`));

const firstyEntry = (table, column) =>
  dbConnection
    .then(() =>
      client
        .execute(
          `SELECT ${column} FROM ${config.keyspace_name}.${table} LIMIT 1;`
        )
        .then((result) => result.rows)
    )
    .catch((err) => console.log(`Error ocurred while trying to get first entry from the table\n${err}`));

module.exports = { tableData, udtInfo, allTables, firstyEntry };
