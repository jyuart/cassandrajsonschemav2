const { Client } = require("cassandra-driver");
const { config } = require("../../config/dbConfig");

const client = new Client({
  contactPoints: [`${config.host}:${config.port}`],
  localDataCenter: config.localDataCenter,
  credentials: {
    username: config.user,
    password: config.password,
  },
  keyspace: config.keyspace_name,
});

const dbConnection = client.connect().then(
  () => console.log("Connection established"),
  (error) => console.log(`Error ocurred while trying to connect\n${error}`)
);

module.exports = { client, dbConnection };