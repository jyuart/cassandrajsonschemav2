const config = {
  host: "localhost",
  port: 9042,
  user: "John",
  password: "qwerty",
  localDataCenter: "datacenter1",
  keyspace_name: "json",
  jsonSchema: "user_result.json"
};

module.exports = { config }